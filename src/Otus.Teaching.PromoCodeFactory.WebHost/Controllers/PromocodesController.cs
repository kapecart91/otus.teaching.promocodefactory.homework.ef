﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<Customer> _customerRepository;
        private IRepository<Preference> _preferenceRepository;
        private IRepository<PromoCode> _promoCodeRepository;

        public PromocodesController(IRepository<Customer> customerRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            return (await _promoCodeRepository.GetAllAsync()).Select(p => new PromoCodeShortResponse
            {
                BeginDate = p.BeginDate.ToShortDateString(),
                EndDate = p.EndDate.ToShortDateString(),
                Code = p.Code,
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo,
                Id = p.Id
            }).ToList();
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            DateTime beginDate = DateTime.MinValue,
                     endDate = DateTime.MinValue;

            DateTime.TryParse(request.BeginDate, out beginDate);
            DateTime.TryParse(request.EndDate, out endDate);

            if (beginDate == DateTime.MinValue && endDate == DateTime.MinValue)
            {
                throw new ArgumentException("Даты переданы в некорректном формате");
            }

            var preference = await _preferenceRepository.GetFirstWhereAsync(p => p.Name == request.Preference);
            var customers = preference.CustomerPreferences.Select(x => x.Customer);

            var promoId = await _promoCodeRepository.CreateAsync(new PromoCode
            {
                BeginDate = beginDate,
                EndDate = endDate,
                Preference = preference,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Code = request.PromoCode
            });

            foreach (var customer in customers)
            {
                customer.PromoCodeId = promoId;
                await _customerRepository.UpdateAsync(customer);
            }
        }
    }
}