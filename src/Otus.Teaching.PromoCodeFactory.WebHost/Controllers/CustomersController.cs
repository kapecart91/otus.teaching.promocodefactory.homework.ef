﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customers, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customers;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            return (await _customerRepository.GetAllAsync()).Select(x => new CustomerShortResponse
            {
                LastName = x.LastName,
                FirstName = x.FirstName,
                Email = x.Email,
                Id = x.Id,
            }).ToList();
        }

        /// <summary>
        /// Получить клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var model = await _customerRepository.GetByIdAsync(id);

            var responce = new CustomerResponse
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                PromoCodes = new List<PromoCodeShortResponse>(),
                Prefernces = model.CustomerPreferences.Select(x => new PrefernceResponse
                {
                    Name = x.Preference.Name
                }).ToList()
            };

            if (model.PromoCode != null)
            {
                var promoCode = model.PromoCode;
                responce.PromoCodes.Add(new PromoCodeShortResponse
                {
                    BeginDate = promoCode.BeginDate.ToShortDateString(),
                    EndDate = promoCode.EndDate.ToShortDateString(),
                    Code = promoCode.Code,
                    PartnerName = promoCode.PartnerName,
                    ServiceInfo = promoCode.ServiceInfo,
                    Id = promoCode.Id
                });
            }

            return responce;
        }

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var model = await Map(request);

            await _customerRepository.CreateAsync(model);

            return model.Id;
        }

        private async Task<Customer> Map(CreateOrEditCustomerRequest request, Customer model = null)
        {
            if (model == null)
            {
                model = new Customer();
            }

            model.Email = request.Email;
            model.FirstName = request.FirstName;
            model.LastName = request.LastName;

            var preferences = await _preferenceRepository.GetRangeByIdsAsync(request.PreferenceIds);

            foreach (var preference in preferences)
            {
                model.CustomerPreferences.Add(new CustomerPreference { Preference = preference });
            }

            return model;
        }

        /// <summary>
        /// Редактирование клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.CustomerPreferences.Clear();

            await Map(request, customer);

            await _customerRepository.UpdateAsync(customer);

            return NoContent();
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.CustomerPreferences.Clear();
            await _customerRepository.DeleteAsync(customer);

            return NoContent();
        }
    }
}