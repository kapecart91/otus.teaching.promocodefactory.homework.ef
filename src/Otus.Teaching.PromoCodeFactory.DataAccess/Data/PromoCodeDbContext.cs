﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDbContext : DbContext
    {
        public PromoCodeDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .Property(x => x.Email)
                .HasMaxLength(100);
            modelBuilder.Entity<Employee>()
                .Property(x => x.FirstName)
                .HasMaxLength(50);
            modelBuilder.Entity<Employee>()
                .Property(x => x.LastName)
                .HasMaxLength(50);

            modelBuilder.Entity<Role>()
                .Property(x => x.Name)
                .HasMaxLength(50);
            modelBuilder.Entity<Role>()
               .Property(x => x.Description)
               .HasMaxLength(250);

            modelBuilder.Entity<Customer>()
                .Property(x => x.Email)
                .HasMaxLength(50);
            modelBuilder.Entity<Customer>()
                .Property(x => x.FirstName)
                .HasMaxLength(50);
            modelBuilder.Entity<Customer>()
                .Property(x => x.LastName)
                .HasMaxLength(50);

            modelBuilder.Entity<Preference>()
                .Property(x => x.Name)
                .HasMaxLength(50);

            modelBuilder.Entity<PromoCode>()
                 .Property(x => x.PartnerName)
                 .HasMaxLength(50);
            modelBuilder.Entity<PromoCode>()
                .Property(x => x.Code)
                .HasMaxLength(30);
            modelBuilder.Entity<PromoCode>()
                .Property(x => x.ServiceInfo)
                .HasMaxLength(250);

            base.OnModelCreating(modelBuilder);
        }

    }
}
