﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class PromoCodeDbContextSeed
    {
        PromoCodeDbContext _context;
        public PromoCodeDbContextSeed(PromoCodeDbContext context)
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
            _context = context;
        }

        public void Seed()
        {
            _context.AddRange(FakeDataFactory.Employees);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.Preferences);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.PromoCodes);
            _context.SaveChanges();
            _context.AddRange(FakeDataFactory.Customers);
            _context.SaveChanges();
        }
    }
}
