﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public Guid? PromoCodeId { get; set; }

        [ForeignKey("PromoCodeId")]
        public virtual PromoCode PromoCode { get; set; }

        public virtual IList<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
    }
}