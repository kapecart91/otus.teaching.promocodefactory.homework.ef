﻿using System;
using System.Runtime;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string PartnerName { get; set; }

        public Guid? PartnerManagerId { get; set; }

        [ForeignKey("PartnerManagerId")]
        public virtual Employee PartnerManager { get; set; }

        public Guid PreferenceId { get; set; }

        [ForeignKey("PreferenceId")]
        public virtual Preference Preference { get; set; }

        public virtual IList<Customer> Customers { get; set; } = new List<Customer>();

    }
}